import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiInicioPage } from './mi-inicio';

@NgModule({
  declarations: [
    MiInicioPage,
  ],
  imports: [
    IonicPageModule.forChild(MiInicioPage),
  ],
})
export class MiInicioPageModule {}
